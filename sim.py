#!/usr/bin/python3

import numpy as np
import scipy as sp
from scipy import sparse
import math
import networkx as nx

from node import Node

from networkx.generators.random_graphs import fast_gnp_random_graph
from numpy.random import rand
from collections import Counter
import timeit

from sim_analysis import compute_infl_friends
from sim_analysis import compute_segregation_measure

max_steps = 500000 #maximum number of time steps per trial
tol = 1e-5 #tolerance for convergence
xmin = 0
xmax = 1


def setup_trial(N,p):
    nodes, edges = generate_ER_network(N,p)
    X = xmin +(xmax-xmin)*rand(N)
    X = np.sort(X) # use if you want to analyze communities
    return X, nodes, edges


# run_fast() is the same as run() without storing timeseries data
def run_fast(N,p,M,K,C,alpha,rewiring_thres,num_trials):
    
    convergence_time = np.empty(num_trials, dtype='int32')
    store_X = np.ndarray((N,num_trials))
    r2 = np.empty(num_trials)
    store_edges = np.empty(num_trials, dtype='object')
    num_discordant_edges = np.ndarray((max_steps,num_trials))
    num_infl = np.zeros((N,int(max_steps/1000),num_trials))
    
    #avg_shortest_path_lengths = np.ndarray((int(max_steps/1000),num_trials), dtype=np.ndarray)
    #components = np.ndarray((int(max_steps/1000),num_trials), dtype=np.ndarray)
    
    discretized_bins = 10 # number of bins for discretized opinion space 
    E_i = np.ndarray((discretized_bins,int(max_steps/1000),num_trials))
    
    
    for trial in range(num_trials):
        if int(rewiring_thres) == 1: rewiring_enabled = False
        else: rewiring_enabled = True
            
        stationary_marker = 0
        stationary_counter = 0
        t_step = 0
        
        X, nodes, edges = setup_trial(N,p)
        
        start_time = timeit.default_timer()
    
        while ((t_step <= max_steps-1) & (stationary_marker != 1)):

            # count number of influences on each node at every other 1000 steps
            if (t_step % 1000 == 0):
                
                # compute number of influential friends
                num_infl[:,int(t_step/1000),trial] = compute_infl_friends(edges,X,C,N)
            
                # compute measure of segregation, see Henry et al.
                E_i[:,int(t_step/1000),trial] = compute_segregation_measure(edges,X,discretized_bins)
                
            discordant_edges = compute_discordant_edges(edges,X,rewiring_thres)
            m_discordant = len(discordant_edges)
            num_discordant_edges[t_step,trial] = m_discordant
            
            if ((m_discordant != 0) & (rewiring_enabled)):

                if m_discordant < M: 
                    rewiring_eix = list(np.random.choice(range(m_discordant), size=m_discordant, replace=False, p=None))
                else: 
                    rewiring_eix = list(np.random.choice(range(m_discordant), size=M, replace=False, p=None))
                
                rewiring_edges = [discordant_edges[eix] for eix in rewiring_eix]
                for edge in rewiring_edges:
                    # dissolve edge
                    edges.remove(edge)
                    # rewire
                    new_edge, nodes = rewire(edge,nodes,X)
                    edges.append(new_edge)
                    
                # update opinions of K pairs of agents
                X_new = update_opinions(edges,K,X,C,alpha)
            
            else:
                
                # otherwise update opinions of M pairs of agents w/o rewiring
                X_new = update_opinions(edges,K,X,C,alpha)

            
            state_change = np.abs(X_new-X)
            if np.sum(state_change) < tol:
                stationary_counter = stationary_counter + 1
            else: stationary_counter = 0
            if stationary_counter == 100:
                stationary_marker = 1

            t_step = t_step+1  # increase time step counter
            X = X_new
        
        convergence_time[trial] = int(t_step)
        X_avg = np.sum(X)/N
        r2[trial] = np.sqrt(np.sum(np.square(X-X_avg))/N)
        store_X[:,trial] = X
        store_edges[trial] = edges
        
        if t_step >= max_steps-1:
            stop_time = timeit.default_timer()
            execution_time = stop_time - start_time
            print("C=" + str(C) + " threshold=" + str(round(rewiring_thres,2)) + " Trial " + str(trial) + " timed out in " + str(round(execution_time,2)) + " seconds.")

        else:
            stop_time = timeit.default_timer()
            execution_time = stop_time - start_time
            print("C=" + str(C) + " threshold=" + str(round(rewiring_thres,2)) + " Trial "+ str(trial) + " finished in " + str(round(execution_time,2)) + " seconds.")
        

    return store_edges, store_X, r2, convergence_time, num_discordant_edges, num_infl, E_i



def run(N,p,M,K,C,alpha,rewiring_thres,num_trials):
    
    convergence_time = np.empty(num_trials, dtype='int32')
    #store_avg = np.ndarray((max_steps,num_trials))
    #store_X = np.ndarray((N,max_steps,num_trials))
    store_X = np.ndarray((N,num_trials))
    r2 = np.empty(num_trials)
    store_edges = np.empty(num_trials, dtype='object')
    rewire_counter = np.zeros((N,num_trials))
    rewire_marker = np.ndarray((max_steps,num_trials))
    num_discordant_edges = np.ndarray((max_steps,num_trials))
    num_infl = np.ndarray((N,int(max_steps/1000),num_trials))
    #components = np.ndarray((int(max_steps/1000),num_trials), dtype=np.ndarray)
        
    discretized_bins = 10 # number of bins for discretized opinion space 
    E_i = np.ndarray((discretized_bins,int(max_steps/1000),num_trials))
    
    
    for trial in range(num_trials):
        if int(rewiring_thres) == 1: rewiring_enabled = False
        else: rewiring_enabled = True
            
        stationary_marker = 0
        stationary_counter = 0
        t_step = 0
        
        X, nodes, edges = setup_trial(N,p)
        edges_seed = edges.copy()
        
        start_time = timeit.default_timer()
    
        while ((t_step <= max_steps-1) & (stationary_marker != 1)):

            #X_avg = np.sum(X)/N  # compute avg. opinion
            #store_avg[t_step,trial] = X_avg  # Store average opinion to plot time series
            #store_X[:,t_step,trial] = X  # store opinion states to plot time series

            discordant_edges = compute_discordant_edges(edges,X,rewiring_thres)
            m_discordant = len(discordant_edges)
            num_discordant_edges[t_step,trial] = m_discordant

            # count number of influences on each node at every other 1000 steps
            if (t_step % 1000 == 0):
                
                # compute number of influential friends
                num_infl[:,int(t_step/1000),trial] = compute_infl_friends(edges,X,C,N)
            
                # compute measure of segregation, see Henry et al.
                E_i[:,int(t_step/1000),trial] = compute_segregation_measure(edges,X,discretized_bins)
                  

            if ((m_discordant != 0) & (rewiring_enabled)):
                
                # pick M random discordant edges
                if m_discordant < M: 
                    rewiring_eix = list(np.random.choice(range(m_discordant), size=m_discordant, replace=False, p=None))
                else: 
                    rewiring_eix = list(np.random.choice(range(m_discordant), size=M, replace=False, p=None))
                
                rewiring_edges = [discordant_edges[eix] for eix in rewiring_eix]

                for edge in rewiring_edges:
                    # dissolve edge
                    edges.remove(edge)
                    # rewire
                    new_edge, nodes = rewire(edge,nodes,X)
                    edges.append(new_edge)
                      
                    #record this rewire 
                    rewire_counter[new_edge[0],trial] = rewire_counter[new_edge[0],trial]+1
                    rewire_marker[t_step,trial] = new_edge[0]

                    
                # update opinions of K pairs of agents
                X_new = update_opinions(edges,K,X,C,alpha)
            
            else:
                
                # otherwise update opinions of M pairs of agents w/o rewiring
                X_new = update_opinions(edges,K,X,C,alpha)
                
                rewire_marker[t_step,trial] = -1 # record that no rewire happens
                

            state_change = np.abs(X_new-X)
            if np.sum(state_change) < tol:
                stationary_counter = stationary_counter + 1
            else: stationary_counter = 0
            if stationary_counter == 100:
                stationary_marker = 1

            t_step = t_step+1  # increase time step counter
            X = X_new
        
        convergence_time[trial] = int(t_step)
        X_avg = np.sum(X)/N
        r2[trial] = np.sqrt(np.sum(np.square(X-X_avg))/N)
        store_X[:,trial] = X
        store_edges[trial] = edges
        
        if t_step >= max_steps-1:
            stop_time = timeit.default_timer()
            execution_time = stop_time - start_time
            print("C=" + str(C) + " threshold=" + str(round(rewiring_thres,2)) + " Trial " + str(trial) + " timed out in " + str(round(execution_time,2)) + " seconds.")

        else:
            stop_time = timeit.default_timer()
            execution_time = stop_time - start_time
            print("C=" + str(C) + " threshold=" + str(round(rewiring_thres,2)) + " Trial "+ str(trial) + " finished in " + str(round(execution_time,2)) + " seconds.")

        
    return store_edges, store_X, r2, convergence_time, rewire_counter, rewire_marker, num_discordant_edges, num_infl, E_i, avg_degree



def generate_ER_network(N,p):
    G = fast_gnp_random_graph(n=N,p=p,seed=None,directed=False)
    edges = [(u,v) for u,v in G.edges()]
    nodes = []
    
    for i in range(N):
        n_neigh = list(G[i])
        n = Node(n_neigh)
        nodes.append(n)
        
    return nodes, edges



def compute_rewiring_prob_distr(x,X,nodes):
    X_x = X[x]
    d_xz = np.array([(X_x - X_z)**2 for X_z in X])

    rewiring_distr = 1 - d_xz
    rewiring_distr[x] = 0 # set probability for x->x to 0 to prevent self-loop
    rewiring_distr[nodes[x].neighbors] = 0 # prevent duplicate edges and length-2 cycles

    A = np.sum(rewiring_distr)
    rewiring_distr = (1/A) * rewiring_distr # normalize the distribution
    
    return rewiring_distr


def rewire(edge,nodes,X):

    i = edge[0]
    j = edge[1]

    # remove each other from being neighbors
    nodes[i].erase_neigh(j)
    nodes[j].erase_neigh(i)

    # select either i or j to rewire with equal probability
    random_node_selector = np.random.randint(0,2)
    if random_node_selector == 0: 
        x = i
    else: 
        x = j

    rewiring_distr = compute_rewiring_prob_distr(x,X,nodes)

    # pick a new node to rewire to
    z = int(np.random.choice(range(len(nodes)), size=1, replace=False, p=rewiring_distr))
    new_edge = (x,z)
    nodes[x].add_neigh(z)
    nodes[z].add_neigh(x)

    return new_edge, nodes



def update_opinions(edges,K,X,C,alpha):
    
    X_new = X.copy()

    eix = np.random.choice(range(len(edges)), size=K, replace=False)
    edge_pairs = [edges[i] for i in eix]

    for x,y in edge_pairs:
        X_x = X[x]
        X_y = X[y]

        if abs(X_x - X_y) <= C:
            X_new[x] = X_x + alpha*(X_y - X_x)
            X_new[y] = X_y + alpha*(X_x - X_y)

    return X_new



def compute_discordant_edges(edges,X, rewiring_thres):

    return [(i,j) for (i,j) in edges if abs(X[i] - X[j]) > rewiring_thres]



def check_if_neighbor(i,j):
    if j in nodes[i].neighbors: return True
    else: return False





