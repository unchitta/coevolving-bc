#!/usr/bin/python3

import numpy as np
#import matplotlib.pyplot as plt
import scipy
from scipy import sparse
from scipy.stats import norm
from scipy.optimize import curve_fit
import math
import networkx as nx
from joblib import Parallel, delayed
from itertools import product
from collections import Counter


def count_clusters(edges, X, C, N):
    bounded_edges = []
    for (i,j) in edges:
        if abs(X[i] - X[j]) < C:
            bounded_edges.append((i,j))

    G_bounded = nx.Graph()
    G_bounded.add_nodes_from(range(N))
    G_bounded.add_edges_from(bounded_edges)
    
    clusters = [len(c) for c in sorted(nx.connected_components(G_bounded), key=len, reverse=True)]
    num_major = sum(1 for c in clusters if c >= 50)
    num_minor = len(clusters) - num_major
    return clusters, num_major, num_minor


def compute_infl_friends(edges,X,C,N):
    num_infl = np.zeros(N, dtype=int)

    infl_edges = [e for e in edges if abs(X[e[0]]-X[e[1]]) <= C]
    cnt1 = Counter(edge[0] for edge in infl_edges)
    cnt2 = Counter(edge[1] for edge in infl_edges)
    cnt = cnt1 + cnt2

    for i in cnt.elements():
        num_infl[i] = cnt[i]

    return num_infl


def compute_segregation_measure(edges,X,discretized_bins=10):
    E_i = []
    E = [ round( np.ceil( discretized_bins * abs(X[i] - X[j]) ) / discretized_bins, 1 ) if abs(X[i] - X[j]) > 0 else 1/discretized_bins for (i,j) in edges]
    E_counter = Counter(E)
    
    E_i = [E_counter[round(j,1)] for i,j in enumerate(np.arange(0.1,1.1,0.1))]
        
    return E_i



def draw_network(graph, pos, X, ax):
    #plt.figure(dpi=150)
    
    # draw nodes
    nx.draw_networkx_nodes(graph,pos, nodelist=list(np.argwhere((X < 0.2)).flatten()),
                           node_color='g',node_size=50,alpha=0.5,label='< 0.2',ax=ax)


    nx.draw_networkx_nodes(graph,pos,nodelist=list(np.argwhere((X >= 0.2) & (X < 0.4)).flatten()),
                           node_color='turquoise',node_size=50,alpha=0.5,label='0.2 <= x < 0.4',ax=ax)

    nx.draw_networkx_nodes(graph,pos,nodelist=list(np.argwhere((X >= 0.4) & (X <= 0.6)).flatten()),
                           node_color='orange',node_size=50,alpha=0.5,label='0.4 <= x <= 0.6',ax=ax)

    nx.draw_networkx_nodes(graph,pos,nodelist=list(np.argwhere((X > 0.6) & (X <= 0.8)).flatten()),
                           node_color='purple',node_size=50,alpha=0.5,label='0.6 < x <= 0.8',ax=ax)
    nx.draw_networkx_nodes(graph,pos,nodelist=list(np.argwhere((X > 0.8)).flatten()),
                           node_color='r',node_size=50,alpha=0.5,label='> 0.8',ax=ax)

    # draw edges
    nx.draw_networkx_edges(graph,pos,width=0.5,alpha=0.25,ax=ax)


def gaussian(x, A, mu, sigma):
	#return normalizing_const * np.exp(-(x-mu)**2/(2*sigma**2))
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))

def compute_normal_pdfs(matrix, num_bins=50):
    
	padded_rows = []
    
	for i in range(matrix.shape[0]):
		pad_top = sparse.csr_matrix(np.zeros(1000-i))
		pad_bottom = sparse.csr_matrix(np.zeros(i))
		padded = scipy.sparse.hstack([pad_top,matrix[i,:],pad_bottom])
		padded_rows.append(padded)
        
	stacked = scipy.sparse.vstack(padded_rows)
	sum_all = np.ravel(np.sum(stacked.todense(), axis=0))
	summed = sum_all.tolist()
	bin_width = int(round(len(summed) / num_bins))
	binned = [sum(summed[i:i+bin_width]) for i in range(0, len(summed), bin_width)]
	p0=[1000,25,10]
	popt, pcov = curve_fit(gaussian, range(len(binned)), binned, p0=p0)

	#popt, pcov = curve_fit(gaussian, range(2000), summed)
	
	return popt, pcov, binned


def compute_band_width(adj):

	'''returns mu and standard deviation of band width given an adjacency matrix'''
	popt, _, _ = compute_normal_pdfs(adj)
	mu = popt[1]
	std = popt[2]
        
	return mu, std


def compute_band_widths(N,p,rewiring_thresholds, C, alpha=0.1, M=5, n_sim=1):

	'''returns mu and standard deviation of band width in the spyplot of 
	n_sim simulations for each value of rewiring threshold'''

	mu = []
	stdev = []

	for thres in rewiring_thresholds:

		temp_mu = []
		temp_std = []

		for i in range(n_sim):
			_, _, edges, _, _, _, _, _, _, _, _ = sim.run(N,p,M,C,alpha,thres,1)
			G = nx.Graph()
			G.add_nodes_from(range(N))
			G.add_edges_from(edges)
			adj = nx.adjacency_matrix(G)
			popt, _, _ = compute_normal_pdfs(adj)
			temp_mu.append(popt[1])
			temp_std.append(popt[2])
			print("trial std: " + str(popt[2]))
		mu.append(np.mean(temp_mu))
		stdev.append(np.mean(temp_std))
		print("append mean std: " + str(np.mean(temp_std)))
        
	return mu, stdev

def compute_band_widths_parallel(N,p,rewiring_thresholds, C, alpha=0.1, M=5, n_sim=1):
    
    def _compute_trial_width(N,p,thres, C, alpha, M):
        _, _, edges, _, _, _, _, _, _, _, _ = sim.run(N,p,M,C,alpha,thres,1)
        G = nx.Graph()
        G.add_nodes_from(range(N))
        G.add_edges_from(edges)
        adj = nx.adjacency_matrix(G)
        popt, _, _ = compute_normal_pdfs(adj)
        print("trial std: " + str(popt[2]))
        return popt[2]
    
    
    stdev = []
    
    with Parallel(n_jobs=40, verbose=1) as parallel:
        
        for thres in rewiring_thresholds:
            temp_std = parallel(delayed(_compute_trial_width)(N,p,thres, C, alpha, M) for i in range(n_sim))
            stdev.append(np.mean(temp_std))
            print("append mean std: " + str(np.mean(temp_std)))
    
    return stdev


'''Based on NetworkX 1.1 Implementation for CC subgraphs'''
def connected_component_subgraphs(G, copy=True):
    for c in sorted(nx.connected_components(G), key=len, reverse=True):
        if copy:
            yield G.subgraph(c).copy()
        else:
            yield G.subgraph(c)


def assortativity(G, X):
    
    def _kronecker_delta(i,j):
        if i==j: return 1
        else: return 0
    
    m = G.number_of_edges()
    A = nx.to_numpy_matrix(G)
    N = A.shape[0]
    
    r_numerator = np.sum( [ (A[i,j] - np.sum(A[i,:])*np.sum(A[j,:])/(2*m)) * (X[i]*X[j]) for i,j in product(range(N), range(N)) ] )
    r_denominator = np.sum( [ (np.sum(A[i,:]) * _kronecker_delta(i,j) - np.sum(A[i,:])*np.sum(A[j,:])/(2*m))  * (X[i]*X[j]) for i,j in product(range(N), range(N)) ] ) 

    return r_numerator / r_denominator


def avg_shortest_path(G):
    
    if nx.number_connected_components(G) > 1:
        return [nx.average_shortest_path_length(g) for g in connected_component_subgraphs(G)]
    else:
        return [nx.average_shortest_path_length(G)]
    
    

def avg_local_clustering(G):
    
    if nx.number_connected_components(G) > 1:
        return [nx.average_clustering(g) for g in connected_component_subgraphs(G)]
    else:
        return [nx.average_clustering(G)]
