class Node(object):
    
    def __init__(self, neigh=[]):
        self.neighbors = neigh

    def erase_neigh(self,j):
        self.neighbors.remove(j)

    def add_neigh(self,j):
        self.neighbors.append(j)
        
    def get_neigh(self):
        return self.neighbors