#!/usr/bin/python3

import numpy as np
import networkx as nx

import os
import glob

import sim
from sim_analysis import compute_band_width
from sim_analysis import assortativity
from sim_analysis import avg_local_clustering
from sim_analysis import count_clusters
from joblib import Parallel, delayed


def _run_one_trial(N, p, thres, C, alpha, M, K):
        store_edges, store_X, r2, convergence_time, num_discordant_edges, num_infl, E_i = sim.run_fast(N,p,M,K,C,alpha,thres,1)
        
        edges = store_edges[0]
        steps = convergence_time[0]
        X = store_X[:].flatten()
        last_idx = int((steps - (steps % 1000))/1000)
        G = nx.Graph()
        G.add_nodes_from(range(N))
        G.add_edges_from(edges)
        adj = nx.adjacency_matrix(G)
        
        # compute band width
        _, band_width = compute_band_width(adj)

        # compute opinion clusters
        clusters, num_major, num_minor = count_clusters(edges,X,C,N)
        
        # compute topological properties
        assortativity_coeff = assortativity(G,X)
        local_clustering = avg_local_clustering(G)
        
        return [X, r2[0], steps, band_width, clusters, num_major, num_minor, num_discordant_edges[:steps], num_infl[:,:last_idx,0], E_i[:,:last_idx,0], assortativity_coeff, local_clustering]


def run_trials_parallel(N, p, thres, C, alpha=0.1, M=1, K=5, n_sim=1):
    
    outlist = []
    
    with Parallel(n_jobs=50, verbose=1) as parallel:
        
        #for cee in C:
            #for thres in rewiring_thresholds:
        output = parallel(delayed(_run_one_trial)(N, p, thres, C, alpha, M, K) for i in range(n_sim))
        outlist.append(output)
    
    return outlist


def run_parallel(N,p,rewiring_thresholds, C, alpha=0.1, M=1, K=5, n_sim=1):
    for cee in C:
        for thres in rewiring_thresholds:
            outfile = 'data/05_28_2020/C_0' + str(int(cee*100)) + '_thres_0' + str(int(thres*100)) + '.npy'
            output = run_trials_parallel(N, p, thres, cee, alpha, M, K, n_sim)
            np.save(outfile, output)

            
#=========================

if __name__ == '__main__':
    
    N=1000
    M=1
    K=5
    k=10
    p=k/float(N)

    rewiring_thresholds = np.round(np.arange(0.1,1.02,0.02),3)
    C = np.round(np.arange(0.1,0.302,0.02),3)
    
    run_parallel(N,p,rewiring_thresholds, C, 0.1, M, K, n_sim=50)
    
    print('files submitted')