## About
This is a public repo for an ongoing research project on a coevolving bounded-confidence opinion dynamics model.

---
## File structure
- sim.py : contains function you can call to run one or multiple trials
- run_sim.py : contains function to run multiple trials in parallel and perform some computations afterwards
- sim_analysis: contains helper functions to help with calculations and simulation data analysis
- node.py : contains a class that was used in sim.py

**TO DO:**
- Add model specifications to README.md