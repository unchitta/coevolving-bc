# coevolving-bc

This is a repo for our coevolving bounded-confidence opinion dynamics model (paper in progress; Unchitta Kan, Michelle Feng, Mason Porter).

Developed and maintained by Unchitta Kan.