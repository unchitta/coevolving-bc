import numpy as np
from numpy.random import default_rng, SeedSequence, RandomState, MT19937
import random
import networkx as nx
from networkx.generators.random_graphs import fast_gnp_random_graph
import pickle
import _pickle as cPickle
import bz2

from node import Node

class Model:
	def __init__(self, seedseq, **kwparams):
		# First, set the random state for this model instance
		# This will ensure exact reproducibility as long as the same seedseq is passed
		self.seedseq = seedseq      # set the SeedSequence just for record
		try:
			self.spawn_key = seedseq.spawn_key[0]
		except:
			self.spawn_key = None
		# each instance gets its own PRNG for things like selecting edges at random
		self.rng = default_rng(seedseq)     
		# set the state of the random module (may be used internally by NetworkX)
		random.seed(seedseq)
		self.random_state = RandomState(MT19937(seedseq))

		# Next, set model parameters using kwparams
		self.maxsteps = kwparams['maxsteps']    # bailout time
		self.tolerance = kwparams['tolerance']  # convergence tolerance
		self.alpha = kwparams['alpha']      # convergence parameter
		self.C = kwparams['C']              # confidence bound
		self.beta = kwparams['beta']        # rewiring threshold
		self.N = kwparams['N']              # number of nodes
		self.p = kwparams['p']              # the p in G(N,p)
		self.M = kwparams['M']              # num edges to rewire each step
		self.K = kwparams['K']              # num node pairs to adjust opinions each step
		self.trial = kwparams['trial']      # trial identifer (for saving the model)
		self.fulltimeseries = kwparams['fulltimeseries']        # whether or not to save timeseries opinion data

		# The following function will generate an initial system and set these 5 attributes:
		# self.X, self.initial_X, self.initial_edges, self.nodes, self.edges
		self.__initialize_system()

		# Initialize other attributes to store data
		if self.fulltimeseries:
			self.X_data = np.ndarray((self.maxsteps,self.N))    # for storing opinion time series
			self.X_data[0,:] = self.X           # record the initial opinion profile
			self.edge_changes = []              # for keeping track of edge changes
		else:
			self.X_data = np.ndarray((int(self.maxsteps/500)+1,self.N))    # store opinion time series at every 500 steps
			self.X_data[0,:] = self.X           # record the initial opinion profile
			self.G_snapshots = []        # for storing early-time network snapshots at every 500 steps
		self.num_discordant_edges = np.empty(self.maxsteps)      # for keeping track of number of discordant edges
		self.stationary_counter = 0         # for determining if we've reached stationary state
		self.stationary_marker = 0          # for flagging stationary state
		self.convergence_time = None        # for recording convergence time

		# If beta=1, no rewiring will be done
		self.rewiring_enabled = False if int(kwparams['beta']==1) else True



	def run(self):
		t=0

		#======== INNER HELPER FUNCTIONS TO RUN THE MODEL ========
		def rewire_step():
			# compute discordant edges
			discordant_edges = [(i,j) for (i,j) in self.edges if abs(self.X[i] - self.X[j]) > self.beta]
			self.num_discordant_edges[t] = len(discordant_edges)

			# if len(discordant_edges) >= M, choose M at random using self.rng
			# else, choose all of the discordant edges to rewire
			if len(discordant_edges) > self.M:
				idx = self.rng.choice(a=len(discordant_edges), size=self.M, replace=False)
				edges_to_dissolve = [discordant_edges[i] for i in idx]
			else:
				edges_to_dissolve = discordant_edges

			# dissolve and rewire edges
			for edge in edges_to_dissolve:
				# dissolve edge
				self.edges.remove(edge)
				i = edge[0]; j=edge[1]
				self.nodes[i].erase_neighbor(j)
				self.nodes[j].erase_neighbor(i)

				# pick either i or j to rewire
				random_node_selector = self.rng.integers(2)
				i = i if random_node_selector==0 else j
				selected_node = self.nodes[i]
				new_neighbor = selected_node.rewire(self.X, self.rng)
				self.nodes[new_neighbor].add_neighbor(i)
				new_edge = (i, new_neighbor)

				# record data
				self.edges.append(new_edge)
				if self.fulltimeseries:
					self.edge_changes.append((t, edge, new_edge))

			# Note: I found that the increments for t here are too coarse
			# so for future simulations they should be made finer-grained
			# e.g. in increments of 250
			if (self.fulltimeseries==False) and (t in [500,1000,2000,5000,7500,10000,15000,20000]):
				G = nx.Graph()
				G.add_nodes_from(range(self.N))
				G.add_edges_from(self.edges)
				self.G_snapshots.append((t, G))
						


		def dw_step():
			# pick K node pairs at random using self.rng
			idx = self.rng.integers(low=0, high=len(self.edges), size=self.K)
			nodepairs = [self.edges[i] for i in idx]

			# for each pair, update opinions both at the model level and node level
			X_new = self.X.copy()
			for u,w in nodepairs:
				if abs(self.X[u] - self.X[w]) <= self.C:
					X_new[u] = self.X[u] + self.alpha*(self.X[w] - self.X[u])
					X_new[w] = self.X[w] + self.alpha*(self.X[u] - self.X[w])
					self.nodes[u].update_opinion(X_new[u], w)
					self.nodes[w].update_opinion(X_new[w], u)
			
			# update data
			self.X_prev = self.X.copy()
			self.X = X_new
			if self.fulltimeseries: 
				self.X_data[t+1,:] = X_new
			elif (t%500==0):
				t_prime = int(t/500)
				self.X_data[t_prime+1] = X_new


		def check_convergence():
			state_change = np.sum(np.abs(self.X - self.X_prev))
			self.stationary_counter = self.stationary_counter+1 if state_change < self.tolerance else 0
			self.stationary_marker = 1 if self.stationary_counter >= 100 else 0


		#==================== ACTUALLY RUN THE MODEL ====================

		while ((t < self.maxsteps-1) & (self.stationary_marker != 1)):
			if self.rewiring_enabled:
				rewire_step()
			dw_step()
			check_convergence()
			t+=1

		self.convergence_time = t
		self.save_model()



	# ===================================================
	# AREA FOR PRIVATE HELPER FUNCTIONS BELOW
	# ===================================================

	
	def __initialize_system(self):
		"""
		This helper function is for generating an initial system of N individuals 
		with uniformly-distributed opinion drawn using self.rng and random connections 
		among individuals are created using NetworkX and self.random_state.

		Sets the following attributes:
		- self.X : an array of length N
		- self.initial_edges : a list of tuples of edges
		- self.nodes : a list of length N containing Node objects
		- self.edges : a list of tuples of edges
		"""

		# draw initial opinions from Unif[0,1] using the rng of the caller object
		X = self.rng.random(self.N)
		# generate a G(N,p) random graph using the random state of the caller object
		G = nx.fast_gnp_random_graph(n=self.N, p=self.p, seed=self.random_state, directed=False)

		# initialize Node objects and store them in a container
		nodes = []
		for i in range(self.N):
			n_neigh = list(G[i])
			n = Node(id=i, initial_opinion=X[i], neighbors=n_neigh)
			nodes.append(n)
		
		# get list of edges
		edges = [(u,v) for u,v in G.edges()]

		# set these values as attributes of self
		self.X = X
		self.initial_X = X
		self.initial_edges = edges.copy()
		self.nodes = nodes
		self.edges = edges.copy()


	# ===================================================
	# AREA FOR PUBLIC HELPER FUNCTIONS BELOW
	# ===================================================

	def get_edges(self, t=None):
		"""Returns a list of edges in the system's network at time t.
		For example, if t==1, then this returns the edges after the first round of rewiring has been executed.
		If t is None or is greater than the convergence time, the method returns the most recent snapshot.
		This method should only be used after the method run() has been called already.
		If the model was set to not save timeseries data, this will return the most recent snapshot.

		Args:
			t (int, optional): Timestep at which to get the network snapshot. Defaults to None.

		Returns:
		   a list of edges in the system at time t.
		"""


		"""
		Note:
		I found a bug where self.timeseries should be self.fulltimeseries (now fixed)
		although this was discovered after simulations have been run, so this function
		is pretty much useless unless (t==None) or (t >= self.convergence_time) for the
		simulation data that I have (new simulation runs won't be affected)

		For the old simulation data, just create a function like this outside of the class
		as a work around.
		"""

		# if t==None, return the current snapshot of the network
		if (t==None) or (t >= self.convergence_time) or (self.timeseries==False):
			return self.edges.copy()
		# elife t==0, return the original network
		elif t==0:
			return self.initial_edges.copy()
		# else, construct a snapshot of the network at time t using the recorded edge changes
		else:
			# first find all the edge changes up until the highest T where T < t
			edges = self.initial_edges.copy()
			edge_changes = [(T,e1,e2) for (T,e1,e2) in self.edge_changes if T<t]
			# then iteratively make changes to the network, starting from initial edges
			for (T,old_edge,new_edge) in edge_changes:
				edges.remove(old_edge)
				edges.append(new_edge)
			return edges


	def get_network(self, t=None):
		"""Returns a NetworkX Graph object that is a snapshot of the system's network at time t.
		For example, if t==1, then this returns the network after the first round of rewiring has been executed.
		If t is None or is greater than the convergence time, the method returns the most recent snapshot.
		This method should only be used after the method run() has been called already.
		If the model was set to not save timeseries data, this will return the most recent snapshot.

		Args:
			t (int, optional): Timestep at which to get the network snapshot. Defaults to None.

		Returns:
			NetworkX Graph object: A graph with N nodes and edges in the system at time t.
		"""

		G = nx.Graph()
		G.add_nodes_from(range(self.N))
		edges = self.get_edges(t)
		G.add_edges_from(edges)
		return G


	def save_model(self):
		"""
		Saves the model in a compressed file format (BZ2) using cPickle.
		"""

		# save only the rows of X_data that have been filled (i.e., remove 0-rows)
		#self.X_data = self.X_data[~np.all(self.X_data == 0, axis=1)]
		if self.fulltimeseries:
			self.X_data = self.X_data[:self.convergence_time, :]
		else:
			self.X_data = self.X_data[:int(self.convergence_time/500)+1, :]
		self.num_discordant_edges = self.num_discordant_edges[:self.convergence_time-1]
		self.num_discordant_edges = np.trim_zeros(self.num_discordant_edges)
		
		C = f"{self.C:.2f}".replace('.','')
		beta = f"{self.beta:.2f}".replace('.','')
		filename = f"data/C_{C}_beta_{beta}_trial_{self.trial}_spk_{self.spawn_key}.pbz2"
		with bz2.BZ2File(filename, 'w') as f: 
			cPickle.dump(self, f)